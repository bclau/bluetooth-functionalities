package com.nc.bttest;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.bluetooth.*;
import android.content.*;
import java.util.*;
import android.util.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.content.Context;


@TargetApi(7)
public class btAcivity extends Activity {
    /** Called when the activity is first created. */
	 final int DISCOVERY_REQUEST = 1;
	 private static final String TAG = "btActivity";
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	final int REQUEST_ENABLE_BT = 0;
    	
    	//ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_list_item_single_choice);
    	final ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        /*
        // retrieve the local Bluetooth device object
        LocalDevice local = null;
        	 
        StreamConnectionNotifier notifier;
        StreamConnection connection = null;
        	 
        	        // setup the server to listen for connection
         try {
                  local = LocalDevice.getLocalDevice();
                  local.setDiscoverable(DiscoveryAgent.GIAC);
        	 
                  UUID uuid = new UUID(80087355); // "04c6093b-0000-1000-8000-00805f9b34fb"
       	          String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
        	            notifier = (StreamConnectionNotifier)Connector.open(url);
         } catch (Exception e) {
        	            e.printStackTrace();
        	            return;
         }
         */
        
        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    	if (mBluetoothAdapter == null) {
    	    // Device does not support Bluetooth
    		Log.v(TAG, "bluetoothadapater is null");
    	}
    	
    	// Enable Bluetooth
    	if (!mBluetoothAdapter.isEnabled()) {
    	    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
    	    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    	}
    	
    	// Display name and address
    	String toastText;
    	if (mBluetoothAdapter.isEnabled()) {
    		String address = mBluetoothAdapter.getAddress(); 
    		String name = mBluetoothAdapter.getName(); 
    		toastText = name + " : " + address;
    		
    	} else
    		toastText = "Bluetooth is not enabled"; 
    	Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
    		
    	//String enableBT = BluetoothAdapter.ACTION_REQUEST_ENABLE; 
    	//startActivityForResult(new Intent(enableBT), 0);
    		String aDiscoverable = BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE; 
        	startActivityForResult(new Intent(aDiscoverable), 0);
        	
        BroadcastReceiver discoveryResult = new BroadcastReceiver() { @Override
        	
        
        		public void onReceive(Context context, Intent intent) {
        			String remoteDeviceName = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
        			BluetoothDevice remoteDevice;
        			remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        			Toast.makeText(getApplicationContext(), "Discovered: " + remoteDeviceName,
        			Toast.LENGTH_SHORT).show();
        		// TODO Do something with the remote Bluetooth Device. 
        		}
        };		
        registerReceiver(discoveryResult,
        		new IntentFilter(BluetoothDevice.ACTION_FOUND)); 
        if (!mBluetoothAdapter.isDiscovering())
        			mBluetoothAdapter.startDiscovery();
    
       registerReceiver(new BroadcastReceiver(){
    	   @Override 
    	   public void onReceive(Context context, Intent intent){
    		   String prevScanMode = BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE;
    		   String sScanMode = BluetoothAdapter.EXTRA_SCAN_MODE;
    		   int iScanMode = intent.getIntExtra(sScanMode, -1);
    		   int prevMode = intent.getIntExtra(prevScanMode, -1);
    	   }
    	   
       },
       new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));

        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE), DISCOVERY_REQUEST);
        
        
        //startActivityForResult(new Intent(aDiscoverable), DISCOVERY_REQUEST);
    	// Querying paired devices
        /*
    	Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
    	// If there are paired devices
    	if (pairedDevices.size() > 0) {
    	    // Loop through paired devices
    	    for (BluetoothDevice device : pairedDevices) {
    	        // Add the name and address to an array adapter to show in a ListView
    	        mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
    	    }
    	}
    	*/

    	// Create a BroadcastReceiver for ACTION_FOUND
    	/*
    	final BroadcastReceiver mReceiver = new BroadcastReceiver() {
    	    public void onReceive(Context context, Intent intent) {
    	        String action = intent.getAction();
    	        // When discovery finds a device
    	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
    	            // Get the BluetoothDevice object from the Intent
    	            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
    	            // Add the name and address to an array adapter to show in a ListView
    	            mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
    	        }
    	    }
    	};
    	*/
    	// Register the BroadcastReceiver
    	/*
    	IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
    	registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy
        */
    
    final class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;
        private String NAME = "bluetoothserver";
        private  final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        public AcceptThread() {
        	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            // Use a temporary object that is later assigned to mmServerSocket,
            // because mmServerSocket is final
            
            try {
            	// BluetoothServerSocket socket = null;
                // MY_UUID is the app's UUID string, also used by the client code
                final BluetoothServerSocket socket = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
                Thread acceptThread = new Thread(new Runnable() { 
                	public void run() {
                		BluetoothSocket serverSocket = socket.accept();
                	}
                	});
                acceptThread.start();
            } catch (IOException e) { }
            mmServerSocket = socket;
        }
        
        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }
                // If a connection was accepted
                if (socket != null) {
                    // Do work to manage the connection (in a separate thread)
                    //manageConnectedSocket(socket);
                    try{
                      mmServerSocket.close();
                    } catch (IOException e) {
                    	break;
                    }
                }
            }
        }
        
        
        /*
        private void manageConnectedSocket(BluetoothSocket mmSocket) {
        	InputStream mmInStream;
            OutputStream mmOutStream;
        	InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;


        }
        */
     
        /** Will cancel the listening socket, and cause the thread to finish */
        /*
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) { }
        }
        */
      }// AcceptThread
    
     final class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
     
        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;
     
            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) { }
            mmSocket = tmp;
        }
     
        public void run() {
            // Cancel discovery because it will slow down the connection
            //mBluetoothAdapter.cancelDiscovery();
     
            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }
     
            // Do work to manage the connection (in a separate thread)
            manageConnectedSocket(mmSocket);
        }
        private void manageConnectedSocket(BluetoothSocket mmSocket) {
        	InputStream mmInStream;
            OutputStream mmOutStream;
        	InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;


        }
     
        /** Will cancel an in-progress connection, and close the socket */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
      }//ConnectThread
    }//onCreate
    @Override 
    protected void onActivityResult(int requestCode, int resultCode, Intent data) { 
    	final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    			if (requestCode == DISCOVERY_REQUEST) {
    				boolean isDiscoverable = resultCode > 0; 
    				int discoverableDuration = resultCode; 
    				if (isDiscoverable) {
    					UUID uuid = UUID.fromString("a60f35f0-b93a-11de-8a39-08002009c666"); 
    					String name = "bluetoothserver";
    					try {
							final BluetoothServerSocket btserver = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(name, uuid);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
    					Thread acceptThread = new Thread(new Runnable() { 
    						public void run() {
    						}
    					}
    				);
    				acceptThread.start(); 
    				}
    			} 
    }
    
}